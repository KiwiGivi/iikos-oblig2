# Obligatorisk oppgave 2 - Bash pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 7.5.a (Prosesser og tr�der)
* 8.6.c (Page faults)
* 8.6.d (En prosess sin bruk av virtuelt og fysisk minne)
* 9.4.a (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

* Ola Eksempel
* Kari Mal

## Kvalitetssikring

Vi brukte [ navn p� verkt�y for linting, statisk analyse eller lignende ] for � kvalitetssikre koden. 

[..] foreslo at vi burde ha fikset p� [..]. Etter � ha rettet opp i dette ved � [..] fikk vi ingen feilmeldinger.


[..] kan installeres p� f�lgende m�te / med f�lgende kommandoer: [..]

For � utf�re eksakt samme kvalitetssikring som vi har gjort, gj�r f�lgende / kj�r f�lgende kommandoer: [..]
